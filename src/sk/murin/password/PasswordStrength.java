package sk.murin.password;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

public class PasswordStrength extends JFrame {

    JButton jbuttonConfirmPassword;
    JTextField jTextFieldPass;
    JLabel labelNumberOfChars;
    JLabel labelMessage;
    final static String[] REGEXES = {".*[0-9].*", ".*[a-z].*", ".*[A-Z].*", ".*\\p{Punct}.*"};
    final static String[] STRENGTH_RATING = {"weak", "medium", "strong", "excellent"};
    final static Color[] COLORS = {Color.red, Color.orange, Color.yellow, Color.green};

    public PasswordStrength() {
        setup();
    }

    private void setup() {
        setTitle("Hraj sa");
        setSize(350, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        add(makePanelTop(), BorderLayout.NORTH);
        add(makePanelCenter(), BorderLayout.CENTER);
        add(makePanelBottom(), BorderLayout.SOUTH);
        setResizable(false);
        setVisible(true);
    }

    private JPanel makePanelTop() {
        JPanel panel = new JPanel();
        JLabel labelLogIn = new JLabel("Log in");
        labelLogIn.setFont(new Font("Courier", Font.BOLD, 15));
        labelLogIn.setBounds(45, 10, 200, 60);
        labelLogIn.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(labelLogIn);
        return panel;
    }

    private JPanel makePanelCenter() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        JLabel labelEnterPassword = new JLabel("Enter your password");
        labelEnterPassword.setFont(new Font("Courier", Font.BOLD, 20));
        labelEnterPassword.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(labelEnterPassword);

        jTextFieldPass = new JTextField();
        jTextFieldPass.setBorder(new LineBorder(Color.black, 2));
        jTextFieldPass.setBounds(45, 100, 250, 60);
        jTextFieldPass.setFont(new Font("Courier", Font.BOLD, 21));

        panel.add(jTextFieldPass);


        jTextFieldPass.setDocument(new JtextFieldCharLimit(15));
        jTextFieldPass.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();

            }
        });

        labelNumberOfChars = new JLabel("0/15");
        labelNumberOfChars.setFont(new Font("Courier", Font.BOLD, 15));
        labelNumberOfChars.setBounds(240, 140, 200, 60);
        panel.add(labelNumberOfChars);
        labelMessage = new JLabel("Weak");
        labelMessage.setForeground(Color.red);
        labelMessage.setBounds(120, 190, 250, 60);
        labelMessage.setFont(new Font("Courier", Font.BOLD, 25));
        panel.add(labelMessage);

        return panel;
    }

    private void countingChars() {
        labelNumberOfChars.setText(jTextFieldPass.getText().length() + "/15");
    }

    private JPanel makePanelBottom() {
        JPanel panel = new JPanel();
        jbuttonConfirmPassword = new JButton("Submit");
        jbuttonConfirmPassword.setFont(new Font("Courier", Font.BOLD, 15));
        jbuttonConfirmPassword.setFocusable(false);
        jbuttonConfirmPassword.setEnabled(false);
        jbuttonConfirmPassword.setBounds(0, 440, 200, 60);
        panel.add(jbuttonConfirmPassword);
        jbuttonConfirmPassword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("nejaky random txt..");
            }
        });
        return panel;
    }

    public boolean isPasswordLong() {
        boolean isLong = jTextFieldPass.getText().length() >= 8;
        jbuttonConfirmPassword.setEnabled(isLong);
        return isLong;
    }

    private void check() {
        String text = jTextFieldPass.getText();
        int score = -1;
        countingChars();
        isPasswordLong();

        for (String regex : REGEXES) {
            if (Pattern.compile(regex).matcher(text).matches()) {
                score++;
            }
        }

        if (isPasswordLong()) {
            labelMessage.setText(STRENGTH_RATING[score]);
            labelMessage.setForeground(COLORS[score]);
        } else {
            labelMessage.setText("Short");
            labelMessage.setForeground(Color.red);
        }
    }
}

